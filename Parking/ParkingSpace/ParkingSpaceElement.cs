﻿namespace Parking.ParkingSpace
{
    public class ParkingSpaceElement : BaseParkingSpase
    {
        public ParkingSpaceElement(int id)
        {
            ID = id;
            IsFree = true;
        }
    }
}
